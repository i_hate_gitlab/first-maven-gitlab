# first-maven
A maven introduction

This codebase will follow [a maven tutorial](https://jenkov.com/tutorials/maven/maven-tutorial.html) by Jakob Jenkov

...and then some :wink:

## Topics
- maven (intro) [tag: first-project](https://github.com/ZagaUS/first-maven/tree/first-project)
- unit testing (intro) see [tag: unit-tests](https://github.com/ZagaUS/first-maven/tree/unit-tests)

## References
- [Maven tutorial](https://jenkov.com/tutorials/maven/maven-tutorial.html)
- [JUnit5 tutorial](https://www.baeldung.com/junit-5)